package com.example.zachary.hoot;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ModifyAssessmentActivity extends AppCompatActivity{

    Assessment assessmentToModify = ModifyAssessmentHelper.getAssessment();
    private EditText title;
    private Switch type;
    private TextView dueDate;
    private TextView goalDate;
    private CheckBox goalDateAlertCheckBox;

    private boolean isObjective;
    private int goalDay, goalMonth, goalYear;
    private int dueDay, dueMonth, dueYear;
    private boolean goalDateAlert;

    Calendar cal = Calendar.getInstance();
    String dateFormat = "MM/dd/yy";
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_assessment_activity);
        title = (EditText) this.findViewById(R.id.titleEditText);
        type = (Switch) this.findViewById(R.id.addTypeToggle);
        dueDate = (TextView) this.findViewById(R.id.dueDateTextView) ;
        goalDate = (TextView) this.findViewById(R.id.goalDateTextView) ;
        goalDateAlertCheckBox = (CheckBox) this.findViewById(R.id.goalDateAlertcheckBox);

        title.setText(assessmentToModify.getTitle());
        if(assessmentToModify.isObjective() == false){
            type.setChecked(true);
        }

        dueDay = assessmentToModify.getDueDay();
        dueMonth = assessmentToModify.getDueMonth();
        dueYear = assessmentToModify.getDueYear();
        goalDay = assessmentToModify.getGoalDay();
        goalMonth = assessmentToModify.getGoalMonth();
        goalYear = assessmentToModify.getGoalYear();

        dueDate.setText(assessmentToModify.getDueMonth()+"/"+assessmentToModify.getDueDay()+"/"+assessmentToModify.getDueYear());
        goalDate.setText(assessmentToModify.getGoalMonth()+"/"+assessmentToModify.getGoalDay()+"/"+assessmentToModify.getGoalYear());

        goalDateAlertCheckBox.setChecked(assessmentToModify.isGoalAlert());

        final DatePickerDialog.OnDateSetListener dueDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dueDay = dayOfMonth;
                dueMonth = monthOfYear+1;
                dueYear = year;
                updateDueDate();
            }
        };

        dueDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(ModifyAssessmentActivity.this, dueDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener goalDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                goalDay = dayOfMonth;
                goalMonth = monthOfYear+1;
                goalYear = year;
                updateGoalDate();
            }
        };

        goalDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(ModifyAssessmentActivity.this, goalDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.modify_assessment_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        assessmentToModify.delete();
        goToAssessmentsActivity();
        return true;
    }

    private void updateDueDate(){
        dueDate.setText(sdf.format(cal.getTime()));
    }
    private void updateGoalDate(){
        goalDate.setText(sdf.format(cal.getTime()));
    }

    public void saveUpdatedAssessment(View v){

        if(type.isChecked()){
            isObjective = false;
        }else{
            isObjective = true;
        }

        goalDateAlert = goalDateAlertCheckBox.isChecked();

        assessmentToModify.setTitle(title.getText().toString());
        assessmentToModify.setObjective(isObjective);
        assessmentToModify.setDueDay(dueDay);
        assessmentToModify.setDueMonth(dueMonth);
        assessmentToModify.setDueYear(dueYear);
        assessmentToModify.setGoalDay(goalDay);
        assessmentToModify.setGoalMonth(goalMonth);
        assessmentToModify.setGoalYear(goalYear);
        assessmentToModify.setGoalAlert(goalDateAlert);
        assessmentToModify.save();
        goToAssessmentsActivity();
    }


    private void goToAssessmentsActivity(){
        Intent myIntent = new Intent(this, AssessmentsActivity.class);
        startActivity(myIntent);
    }

}

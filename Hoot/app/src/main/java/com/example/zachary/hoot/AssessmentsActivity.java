package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AssessmentsActivity extends AppCompatActivity{
    private ListView assessmentsListView;
    private ArrayList<String> assessmentTitles = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assessments_activity);
        assessmentsListView = (ListView) this.findViewById(R.id.assessmentsList);

        List<Assessment> assessments = Assessment.listAll(Assessment.class);

        for(int i = 0; i < assessments.size(); i++){
            String name = assessments.get(i).getTitle();
            assessmentTitles.add(name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, assessmentTitles);
        assessmentsListView.setAdapter(adapter);
        assessmentsListView. setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ModifyAssessmentHelper.setAssessment(((TextView)view).getText().toString());
                Intent myIntent = new Intent(getApplicationContext(), ModifyAssessmentActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.assessments_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(this, AddAssessmentActivity.class);
        startActivity(myIntent);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }
}

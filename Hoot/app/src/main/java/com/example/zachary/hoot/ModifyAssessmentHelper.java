package com.example.zachary.hoot;

import java.util.List;

public class ModifyAssessmentHelper {

    private static Assessment assessment;

    public static Assessment getAssessment() {
        return assessment;
    }

    public static void setAssessment(String assessmentTitle) {
        List<Assessment> assessments = Assessment.listAll(Assessment.class);
        for(int i = 0; i < assessments.size(); i++){
            if(assessmentTitle.equals(assessments.get(i).getTitle())){
                assessment = assessments.get(i);
            }
        }
    }
}

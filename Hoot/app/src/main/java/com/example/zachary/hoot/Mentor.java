package com.example.zachary.hoot;

import com.orm.SugarRecord;

public class Mentor extends SugarRecord {
    private String name;
    private String phoneNumber;
    private String email;
    private int courseId;

    public Mentor(){}

    public Mentor(String name, String phoneNumber, String email) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        courseId = -1;
    }

    public String getName(){
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }
}

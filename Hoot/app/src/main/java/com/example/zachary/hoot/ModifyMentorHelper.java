package com.example.zachary.hoot;

import java.util.List;

public class ModifyMentorHelper {
    private static Mentor mentor;

    public static Mentor getMentor() {
        return mentor;
    }

    public static void setMentor(String mentorName) {
        List<Mentor> mentors = Mentor.listAll(Mentor.class);
        for(int i = 0; i < mentors.size(); i++){
            if(mentorName.equals(mentors.get(i).getName())){
                mentor = mentors.get(i);
            }
        }
    }
}

package com.example.zachary.hoot;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

public class Term extends SugarRecord{

    private int termId;
    private String title;
    private int startDay, startMonth, startYear;
    private int endDay, endMonth, endYear;

    public Term(){}

    public Term(String title, int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear, ArrayList<Course> courses) {
        this.title = title;
        this.startDay = startDay;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.endDay = endDay;
        this.endMonth = endMonth;
        this.endYear = endYear;
        this.termId = Term.listAll(Term.class).size()+1;

        for(int i =0; i < courses.size(); i++){
            courses.get(i).setTermId(termId);
            courses.get(i).save();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTermId() {
        return termId;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public List<Course> getCourses(){
        return Course.find(Course.class, "TERM_ID = ?", String.valueOf(getTermId()));
    }

    public void setCourses(ArrayList<Course> courses) {
        for(int i = 0; i < courses.size(); i++){
            courses.get(i).setTermId(termId);
            courses.get(i).save();
        }

    }
}

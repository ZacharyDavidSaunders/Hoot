package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ModifyMentorActivity extends AppCompatActivity{
    private Button saveButton;
    private EditText name;
    private EditText phone;
    private EditText email;
    private Mentor mentorToModify = ModifyMentorHelper.getMentor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_mentor_activity);
        saveButton = (Button) this.findViewById(R.id.saveButton);
        name = (EditText) this.findViewById(R.id.nameText);
        phone = (EditText) this.findViewById(R.id.phoneText);
        email = (EditText) this.findViewById(R.id.emailText);

        name.setText(mentorToModify.getName());
        phone.setText(mentorToModify.getPhoneNumber());
        email.setText(mentorToModify.getEmail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.modify_mentor_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        mentorToModify.delete();

        Intent myIntent = new Intent(this, MentorsActivity.class);
        startActivity(myIntent);
        return true;
    }

    public void saveUpdatedMentor(View v){
        mentorToModify.setName(name.getText().toString());
        mentorToModify.setEmail(email.getText().toString());
        mentorToModify.setPhoneNumber(phone.getText().toString());
        mentorToModify.save();
        Intent myIntent = new Intent(this, MentorsActivity.class);
        startActivity(myIntent);
    }
}

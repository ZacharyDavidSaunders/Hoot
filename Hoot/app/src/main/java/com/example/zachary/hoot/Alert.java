package com.example.zachary.hoot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.Calendar;
import java.util.List;

public class Alert {

    private static List<Course> allCourses;
    private static List<Assessment> allAssessments;
    static Calendar cal = Calendar.getInstance();
    static int currentDay = cal.get(Calendar.DAY_OF_MONTH);
    static int currentMonth = cal.get(Calendar.MONTH)+1;

    public static void showAlertsIfAny(final Context context){

        allCourses = Course.listAll(Course.class);
        allAssessments = Assessment.listAll(Assessment.class);
        showCourseStartDateAlerts(context);
        showCourseEndDateAlerts(context);
        showAssessmentAlerts(context);
    }

    private static void showCourseStartDateAlerts(Context context){
        String titles = "";
        for(int i = 0; i < allCourses.size(); i++){
            if(allCourses.get(i).isStartDateAlert() && allCourses.get(i).getStartMonth()==currentMonth && allCourses.get(i).getStartDay()==currentDay){
                if(titles.isEmpty()){
                    titles = titles.concat(allCourses.get(i).getTitle());
                }else{
                    titles = titles.concat("/"+allCourses.get(i).getTitle());
                }
            }
        }
        if(!titles.isEmpty()){
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle( "Attention!" )
                    .setIcon(R.mipmap.icon_xxxhdpi)
                    .setMessage("Your "+titles+" course start date is today!\n\n(To disable these alerts, uncheck the start date alert checkboxes on the modify course screens and save.)")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int j) {
                        }
                    }).show();
        }
    }

    private static void showCourseEndDateAlerts(Context context){
        String titles = "";
        for(int i = 0; i < allCourses.size(); i++){
            if(allCourses.get(i).isEndDateAlert() && allCourses.get(i).getEndMonth()==currentMonth && allCourses.get(i).getEndDay()==currentDay){
                if(titles.isEmpty()){
                    titles = titles.concat(allCourses.get(i).getTitle());
                }else{
                    titles = titles.concat("/"+allCourses.get(i).getTitle());
                }
            }
        }
        if(!titles.isEmpty()){
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle( "Attention!" )
                    .setIcon(R.mipmap.icon_xxxhdpi)
                    .setMessage("Your "+titles+" course end date is today!\n\n(To disable these alerts, uncheck the end date alert checkboxes on the modify course screens and save.)")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int j) {
                        }
                    }).show();
        }
    }

    private static void showAssessmentAlerts(Context context){
        String titles = "";
        for(int i = 0; i < allAssessments.size(); i++){
            if(allAssessments.get(i).isGoalAlert() && allAssessments.get(i).getGoalMonth()==currentMonth && allAssessments.get(i).getGoalDay()==currentDay){
                if(titles.isEmpty()){
                    titles = titles.concat(allAssessments.get(i).getTitle());
                }else{
                    titles = titles.concat("/"+allAssessments.get(i).getTitle());
                }
            }
        }
        if(!titles.isEmpty()){
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle( "Attention!" )
                    .setIcon(R.mipmap.icon_xxxhdpi)
                    .setMessage("Your "+titles+" assessment goal day is today!\n\n(To disable these alerts, uncheck the goal date alert checkboxes on the modify assessment screens and save.)")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int j) {
                        }
                    }).show();
        }
    }
}

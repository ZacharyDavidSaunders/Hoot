package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddMentorActivity extends AppCompatActivity{

    private Button saveButton;
    private EditText name;
    private EditText phone;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_mentor_activity);
        saveButton = (Button) this.findViewById(R.id.saveButton);
        name = (EditText) this.findViewById(R.id.nameText);
        phone = (EditText) this.findViewById(R.id.phoneText);
        email = (EditText) this.findViewById(R.id.emailText);
    }

    public void createNewMentor(View v){
        Mentor newMentor = new Mentor(name.getText().toString(), phone.getText().toString(), email.getText().toString());
        newMentor.save();
        goToMentorsActivity();
    }

    private void goToMentorsActivity() {
        Intent myIntent = new Intent(this, MentorsActivity.class);
        startActivity(myIntent);
    }
}

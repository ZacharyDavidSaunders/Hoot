package com.example.zachary.hoot;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AddCourseActivity extends AppCompatActivity{

    private ArrayList<String> mentorInfo = new ArrayList<String>();
    private ArrayList<String> assessmentNames = new ArrayList<String>();

    private Button shareNotesButton;
    private EditText titleEditText, statusEditText, notesEditText;
    private TextView startDateTextView, endDateTextView;
    private CheckBox startDateNotification, endDateNotification;
    private ListView assessmentListView, mentorsListView;

    private int startMonth, startYear, startDay, endMonth, endYear, endDay;

    private Calendar cal = Calendar.getInstance();
    private String dateFormat = "MM/dd/yy";
    private SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_course_activity);

        titleEditText = (EditText) this.findViewById(R.id.titleEditText);
        statusEditText = (EditText) this.findViewById(R.id.statusEditText);
        notesEditText = (EditText) this.findViewById(R.id.notesEditText);
        shareNotesButton = (Button) this.findViewById(R.id.shareNotesButton);
        endDateNotification = (CheckBox) this.findViewById(R.id.endDateNotoficationCheckBox);
        startDateNotification = (CheckBox) this.findViewById(R.id.startDateNotificationcheckBox);
        assessmentListView = (ListView) this.findViewById(R.id.assessmentListView);
        mentorsListView = (ListView) this.findViewById(R.id.mentorsListView);
        startDateTextView = (TextView) this.findViewById(R.id.startDateTextView);
        endDateTextView = (TextView) this.findViewById(R.id.endDateTextView);

        List<Assessment> assessments = Assessment.listAll(Assessment.class);
        List<Mentor> mentors = Assessment.listAll(Mentor.class);

        for(int i = 0; i < assessments.size(); i++){
            String name = assessments.get(i).getTitle();
            assessmentNames.add(name);
        }

        for(int i = 0; i < mentors.size(); i++){
            String name = mentors.get(i).getName();
            String phone = mentors.get(i).getPhoneNumber();
            String email = mentors.get(i).getEmail();
            mentorInfo.add(name+", "+phone+", "+email);
        }

        ArrayAdapter<String> assessmentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, assessmentNames);
        ArrayAdapter<String> mentorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, mentorInfo);
        assessmentListView.setAdapter(assessmentAdapter);
        assessmentListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        assessmentListView.setFastScrollEnabled(true);
        mentorsListView.setAdapter(mentorAdapter);
        mentorsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        mentorsListView.setOnTouchListener(new View.OnTouchListener() {
            // makes the list view work within the scroll view
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        assessmentListView.setOnTouchListener(new View.OnTouchListener() {
            // makes the list view work within the scroll view
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        final DatePickerDialog.OnDateSetListener startDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDay = dayOfMonth;
                startMonth = monthOfYear+1;
                startYear = year;
                updateStartDate();
            }
        };

        startDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddCourseActivity.this, startDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener endDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDay = dayOfMonth;
                endMonth = monthOfYear+1;
                endYear = year;
                updateEndDate();
            }
        };

        endDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddCourseActivity.this, endDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateStartDate(){
        startDateTextView.setText(sdf.format(cal.getTime()));
    }

    private void updateEndDate(){
        endDateTextView.setText(sdf.format(cal.getTime()));
    }

    public void saveNewCourse(View v) {

        ArrayList<Mentor> selectedMentors = new ArrayList<Mentor>();
        ArrayList<Assessment> selectedAssessments = new ArrayList<Assessment>();
        List<Assessment> allAssessments = Assessment.listAll(Assessment.class);
        List<Mentor> allMentors = Mentor.listAll(Mentor.class);

        String title = titleEditText.getText().toString();
        String status = statusEditText.getText().toString();
        String notes = notesEditText.getText().toString();
        boolean startDateAlert = false;
        boolean endDateAlert = false;
        if (startDateNotification.isChecked()) {
            startDateAlert = true;
        }
        if (endDateNotification.isChecked()) {
            endDateAlert = true;
        }

        int assessmentListLength = assessmentListView.getCount();
        SparseBooleanArray checkedAssessments = assessmentListView.getCheckedItemPositions();
        for (int i = 0; i < assessmentListLength; i++){
            if (checkedAssessments.get(i)) {
                String item = assessmentNames.get(i);
                for (int j = 0; j < allAssessments.size(); j++) {
                    if (allAssessments.get(j).getTitle().equals(item.toString())) {
                        selectedAssessments.add(allAssessments.get(j));
                    }
                }
            }
        }

        int mentorListLength = mentorsListView.getCount();
        SparseBooleanArray checkedMentors = mentorsListView.getCheckedItemPositions();
        for (int i = 0; i < mentorListLength; i++){
            if (checkedMentors.get(i)) {
                String item = mentorInfo.get(i);
                for (int j = 0; j < allMentors.size(); j++) {
                    if (item.toString().startsWith(allMentors.get(j).getName())) {
                        selectedMentors.add(allMentors.get(j));
                    }
                }
            }
        }

        Course course = new Course(title, startMonth, startYear, startDay, endMonth, endDay, endYear, startDateAlert, endDateAlert, status, selectedMentors, notes, selectedAssessments);
        course.save();

        Intent myIntent = new Intent(getApplicationContext(), CoursesActivity.class);
        startActivity(myIntent);
    }

    public void shareNotes(View v){
        Share.shareString(this,notesEditText.getText().toString());
    }
}

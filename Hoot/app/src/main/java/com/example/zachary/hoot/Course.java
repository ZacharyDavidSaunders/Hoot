package com.example.zachary.hoot;

import com.orm.SugarRecord;
import java.util.ArrayList;
import java.util.List;

public class Course extends SugarRecord{

    private int courseId;
    private String title;
    private int startMonth, startYear, startDay;
    private int endMonth, endDay, endYear;
    private boolean startDateAlert;
    private boolean endDateAlert;
    private String status;
    private String notes;
    private int termId;

    public Course(){}

    public Course(String title, int startMonth, int startYear, int startDay, int endMonth, int endDay, int endYear, boolean startDateAlert, boolean endDateAlert, String status, ArrayList<Mentor> mentors, String notes, ArrayList<Assessment> assessments) {
        this.title = title;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.startDay = startDay;
        this.endMonth = endMonth;
        this.endDay = endDay;
        this.endYear = endYear;
        this.startDateAlert = startDateAlert;
        this.endDateAlert = endDateAlert;
        this.status = status;
        this.notes = notes;
        this.termId = -1;
        this.courseId = Course.listAll(Course.class).size()+1;
        for(int i = 0; i < mentors.size(); i++){
            Mentor mentor = mentors.get(i);
            mentor.setCourseId(courseId);
            mentor.save();
        }

        for(int i = 0; i < assessments.size(); i++){
            Assessment assessment = assessments.get(i);
            assessment.setCourseId(courseId);
            assessment.save();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public boolean isStartDateAlert() {
        return startDateAlert;
    }

    public void setStartDateAlert(boolean startDateAlert) {
        this.startDateAlert = startDateAlert;
    }

    public boolean isEndDateAlert() {
        return endDateAlert;
    }

    public void setEndDateAlert(boolean endDateAlert) {
        this.endDateAlert = endDateAlert;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Mentor> getMentors() {
        return Mentor.find(Mentor.class, "COURSE_ID = ?", String.valueOf(getCourseId()));
    }

    public void setMentors(ArrayList<Mentor> mentors) {
        for(int i = 0; i < mentors.size(); i++){
            mentors.get(i).setCourseId(courseId);
            mentors.get(i).save();
        }
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Assessment> getAssessments() {
        return Assessment.find(Assessment.class, "COURSE_ID = ?", String.valueOf(getCourseId()));
    }

    public void setAssessments(ArrayList<Assessment> assessments) {
        for(int i = 0; i < assessments.size(); i++){
            assessments.get(i).setCourseId(courseId);
            assessments.get(i).save();
        }
    }

    public int getCourseId(){
       return courseId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }
}

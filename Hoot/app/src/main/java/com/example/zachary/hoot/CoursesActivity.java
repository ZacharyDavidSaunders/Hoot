package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CoursesActivity extends AppCompatActivity{
    private ListView coursesListView;
    private ArrayList<String> coursesNames = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.courses_activity);
        coursesListView = (ListView) this.findViewById(R.id.coursesList);

        List<Course> courses = Course.listAll(Course.class);

        for(int i = 0; i < courses.size(); i++){
            String name = courses.get(i).getTitle();
            coursesNames.add(name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, coursesNames);
        coursesListView.setAdapter(adapter);
        coursesListView. setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ModifyCourseHelper.setCourse(((TextView)view).getText().toString());
                Intent myIntent = new Intent(getApplicationContext(), ModifyCourseActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.courses_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(this, AddCourseActivity.class);
        startActivity(myIntent);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }

}

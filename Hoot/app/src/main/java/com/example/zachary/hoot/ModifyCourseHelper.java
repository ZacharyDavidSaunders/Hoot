package com.example.zachary.hoot;

import java.util.List;

public class ModifyCourseHelper {

    private static Course course;

    public static Course getCourse() {
        return course;
    }

    public static void setCourse(String coursetitle) {
        List<Course> courses = Course.listAll(Course.class);
        for(int i = 0; i < courses.size(); i++){
            if(coursetitle.equals(courses.get(i).getTitle())){
                course = courses.get(i);
            }
        }
    }
}

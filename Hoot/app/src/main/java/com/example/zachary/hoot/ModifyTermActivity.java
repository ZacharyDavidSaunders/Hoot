package com.example.zachary.hoot;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ModifyTermActivity extends AppCompatActivity{

    Term termToBeModified = ModifyTermHelper.getTerm();

    EditText titleEditText;
    TextView startDateTextView;
    TextView endDateTextView;
    ListView coursesListView;
    TextView errorMessage;

    private Calendar cal = Calendar.getInstance();
    private String dateFormat = "MM/dd/yy";
    private SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

    private ArrayList<String> courseTitles = new ArrayList<String>();

    private int startDay, startMonth, startYear;
    private int endDay, endMonth, endYear;
    private boolean termHasNoCourses = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_term_activity);

        titleEditText = (EditText) this.findViewById(R.id.titleEditText);
        startDateTextView = (TextView) this.findViewById(R.id.startDateTextView);
        endDateTextView = (TextView) this.findViewById(R.id.endDateTextView);
        coursesListView = (ListView) this.findViewById(R.id.courseListView);
        errorMessage = (TextView) this.findViewById(R.id.errorMessageTextView);

        startDay = termToBeModified.getStartDay();
        startMonth = termToBeModified.getStartMonth();
        startYear = termToBeModified.getStartYear();
        endDay = termToBeModified.getEndDay();
        endMonth = termToBeModified.getEndMonth();
        endYear = termToBeModified.getEndYear();

        titleEditText.setText(termToBeModified.getTitle());
        startDateTextView.setText(termToBeModified.getStartMonth()+"/"+termToBeModified.getStartDay()+"/"+termToBeModified.getStartYear());
        endDateTextView.setText(termToBeModified.getEndMonth()+"/"+termToBeModified.getEndDay()+"/"+termToBeModified.getEndYear());

        List<Course> validCourses = Course.find(Course.class, "TERM_ID = ?", String.valueOf(-1));
        List<Course> currentCourses = Course.find(Course.class, "TERM_ID = ?", String.valueOf(termToBeModified.getTermId()));
        for(int i = 0; i < validCourses.size(); i++){
            String name = validCourses.get(i).getTitle();
            courseTitles.add(name);
        }

        for(int i = 0; i < currentCourses.size(); i++){
            String name = currentCourses.get(i).getTitle();
            courseTitles.add(name);
        }

        ArrayAdapter<String> coursesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, courseTitles);
        coursesListView.setAdapter(coursesAdapter);
        coursesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        coursesListView.setOnTouchListener(new View.OnTouchListener() {
            // makes the list view work within the scroll view
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        final DatePickerDialog.OnDateSetListener startDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDay = dayOfMonth;
                startMonth = monthOfYear+1;
                startYear = year;
                updateStartDate();
            }
        };

        startDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(ModifyTermActivity.this, startDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener endDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDay = dayOfMonth;
                endMonth = monthOfYear+1;
                endYear = year;
                updateEndDate();
            }
        };

        endDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(ModifyTermActivity.this, endDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        for(int i = 0; i < coursesListView.getCount(); i++){
            String title = coursesAdapter.getItem(i);
            for(int j = 0; j < termToBeModified.getCourses().size(); j++){
                if(title.equals(termToBeModified.getCourses().get(j).getTitle())){
                    coursesListView.setItemChecked(i, true);
                }
            }
        }

        if(termToBeModified.getCourses().size() == 0){
            termHasNoCourses = true;
        }

    }

    private void updateStartDate(){
        startDateTextView.setText(sdf.format(cal.getTime()));
    }

    private void updateEndDate(){
        endDateTextView.setText(sdf.format(cal.getTime()));
    }

    public void saveUpdatedTerm(View v){

        ArrayList<Course> selectedCourses = new ArrayList<Course>();
        ArrayList<Course> notSelectedCourses = new ArrayList<Course>();
        ArrayList <Course> eligibleCourses = new ArrayList<Course>();


        ListAdapter adapter = coursesListView.getAdapter();
        List<Course> allCourses = Course.listAll(Course.class);
        for(int i =0; i < adapter.getCount(); i++){
            Course course = resolveCourseObjectByTitle(adapter.getItem(i).toString(), allCourses);
            eligibleCourses.add(course);
        }


        SparseBooleanArray checkedCourses = coursesListView.getCheckedItemPositions();
        for (int i = 0; i < coursesListView.getCount(); i++){
            String item = courseTitles.get(i);
            Course course = resolveCourseObjectByTitle(item.toString(), eligibleCourses);
            if(checkedCourses.get(i)){
                selectedCourses.add(course);
            }else{
                notSelectedCourses.add(course);
            }
        }

        System.out.println("Selected Courses:\n");
        for(int i =0; i < selectedCourses.size(); i++){
            System.out.println(selectedCourses.get(i).getTitle());
        }

        System.out.println("Not-Selected Courses:\n");
        for(int i =0; i < notSelectedCourses.size(); i++){
            System.out.println(notSelectedCourses.get(i).getTitle());
        }

        termToBeModified.setTitle(titleEditText.getText().toString());
        termToBeModified.setStartDay(startDay);
        termToBeModified.setStartMonth(startMonth);
        termToBeModified.setStartYear(startYear);
        termToBeModified.setEndDay(endDay);
        termToBeModified.setEndMonth(endMonth);
        termToBeModified.setEndYear(endYear);
        termToBeModified.setCourses(selectedCourses);
        termToBeModified.save();

        for(int i = 0; i < notSelectedCourses.size(); i++){
            notSelectedCourses.get(i).setTermId(-1);
            notSelectedCourses.get(i).save();
        }

        goToTermsActivity();
    }

    private Course resolveCourseObjectByTitle(String title, List<Course> possibleCourses){
        for(int i =0; i < possibleCourses.size(); i++){
            if(possibleCourses.get(i).getTitle().equals(title)){
                return possibleCourses.get(i);
            }
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.modify_term_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(termHasNoCourses){
            termToBeModified.delete();
            goToTermsActivity();
        }else{
            errorMessage.setVisibility(View.VISIBLE);
        }

        return true;
    }

    private void goToTermsActivity(){
        Intent myIntent = new Intent(getApplicationContext(), TermsActivity.class);
        startActivity(myIntent);
    }
}

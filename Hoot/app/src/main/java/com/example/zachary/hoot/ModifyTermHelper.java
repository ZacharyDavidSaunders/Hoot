package com.example.zachary.hoot;

import java.util.List;

public class ModifyTermHelper {

    private static Term term;

    public static Term getTerm() {
        return term;
    }

    public static void setTerm(String termInfo) {
        List<Term> allTerms =  Term.listAll(Term.class);
        for(int i = 0; i < allTerms.size(); i++){
            if(termInfo.startsWith(allTerms.get(i).getTitle())){
                ModifyTermHelper.term = allTerms.get(i);
            }
        }
    }
}

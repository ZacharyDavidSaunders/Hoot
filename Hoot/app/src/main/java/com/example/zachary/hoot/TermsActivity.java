package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TermsActivity extends AppCompatActivity {

    private ListView termsListView;
    private List<Term> terms = Term.listAll(Term.class);
    private ArrayList<String> termInfo = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_activity);
        termsListView = (ListView) this.findViewById(R.id.termsList);

        for(int i = 0; i < terms.size(); i++){
            String info = terms.get(i).getTitle()+", "+terms.get(i).getStartMonth()+"/"+terms.get(i).getStartDay()+"/"+terms.get(i).getStartYear()+ " - "+terms.get(i).getEndMonth()+"/"+terms.get(i).getEndDay()+"/"+terms.get(i).getEndYear();
            termInfo.add(info);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, termInfo);
        termsListView.setAdapter(adapter);
        termsListView. setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ModifyTermHelper.setTerm(((TextView)view).getText().toString());
                Intent myIntent = new Intent(getApplicationContext(), ModifyTermActivity.class);
                startActivity(myIntent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.terms_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(this, AddTermActivity.class);
        startActivity(myIntent);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }

}

package com.example.zachary.hoot;

import com.orm.SugarRecord;

public class Assessment extends SugarRecord{

    int courseId;
    String title;
    boolean objective; //0: objective. 1: performance
    int goalDay, goalMonth, goalYear;
    int dueDay, dueMonth, dueYear;
    boolean goalAlert;

    public Assessment(){}

    public Assessment(String title, boolean objective, int goalDay, int goalMonth, int goalYear, int dueDay, int dueMonth, int dueYear, boolean goalAlert) {
        this.title = title;
        this.objective = objective;
        this.goalDay = goalDay;
        this.goalMonth = goalMonth;
        this.goalYear = goalYear;
        this.dueDay = dueDay;
        this.dueMonth = dueMonth;
        this.dueYear = dueYear;
        this.courseId = -1;
        this.goalAlert = goalAlert;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isObjective() {
        return objective;
    }

    public void setObjective(boolean objective) {
        this.objective = objective;
    }

    public int getGoalDay() {
        return goalDay;
    }

    public void setGoalDay(int goalDay) {
        this.goalDay = goalDay;
    }

    public int getGoalMonth() {
        return goalMonth;
    }

    public void setGoalMonth(int goalMonth) {
        this.goalMonth = goalMonth;
    }

    public int getGoalYear() {
        return goalYear;
    }

    public void setGoalYear(int goalYear) {
        this.goalYear = goalYear;
    }

    public int getDueDay() {
        return dueDay;
    }

    public void setDueDay(int dueDay) {
        this.dueDay = dueDay;
    }

    public int getDueMonth() {
        return dueMonth;
    }

    public void setDueMonth(int dueMonth) {
        this.dueMonth = dueMonth;
    }

    public int getDueYear() {
        return dueYear;
    }

    public void setDueYear(int dueYear) {
        this.dueYear = dueYear;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public boolean isGoalAlert() {
        return goalAlert;
    }

    public void setGoalAlert(boolean goalAlert) {
        this.goalAlert = goalAlert;
    }
}

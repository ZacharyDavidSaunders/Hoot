package com.example.zachary.hoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MentorsActivity extends AppCompatActivity {

    private ListView mentorsListView;
    private ArrayList <String> mentorNames = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mentors_activity);
        mentorsListView = (ListView) this.findViewById(R.id.mentorsList);

        List<Mentor> mentors = Mentor.listAll(Mentor.class);

        for(int i = 0; i < mentors.size(); i++){
            String name = mentors.get(i).getName();
            mentorNames.add(name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mentorNames);
        mentorsListView.setAdapter(adapter);
        mentorsListView. setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ModifyMentorHelper.setMentor(((TextView)view).getText().toString());
                Intent myIntent = new Intent(getApplicationContext(), ModifyMentorActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mentors_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(this, AddMentorActivity.class);
        startActivity(myIntent);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }
}

package com.example.zachary.hoot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button mentorsButton,coursesButton,termsButton;
    TextView versionTextView;
    final String SHARED_PREF_FILE = "HootPrefs";
    String version = "1.0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mentorsButton = (Button) this.findViewById(R.id.mentorsButton);
        coursesButton= (Button) this.findViewById(R.id.coursesButton);
        termsButton = (Button) this.findViewById(R.id.termsButton);
        versionTextView = (TextView) this.findViewById(R.id.versionTextView);

        SharedPreferences.Editor editor = getSharedPreferences(SHARED_PREF_FILE, MODE_PRIVATE).edit();
        editor.putString("applicationVersion", version);
        editor.apply();

        SharedPreferences prefs = getSharedPreferences(SHARED_PREF_FILE, MODE_PRIVATE);
        String sharedPreferenceVersionData = prefs.getString("applicationVersion", null);
        versionTextView.setText("VERSION: "+sharedPreferenceVersionData);

        Alert.showAlertsIfAny(this);
    }

    public void goToMentorsActivity(View v){
        Intent myIntent = new Intent(this, MentorsActivity.class);
        startActivity(myIntent);
    }

    public void goToAssessmentsActivity(View v){
        Intent myIntent = new Intent(this, AssessmentsActivity.class);
        startActivity(myIntent);
    }

    public void goToCoursesActivity(View v){
        Intent myIntent = new Intent(this, CoursesActivity.class);
        startActivity(myIntent);
    }

    public void goToTermsActivity(View v){
        Intent myIntent = new Intent(this, TermsActivity.class);
        startActivity(myIntent);
    }
}

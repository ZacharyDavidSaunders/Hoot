package com.example.zachary.hoot;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AddTermActivity extends AppCompatActivity {

    EditText titleEditText;
    TextView startDateTextView;
    TextView endDateTextView;
    ListView coursesListView;

    private Calendar cal = Calendar.getInstance();
    private String dateFormat = "MM/dd/yy";
    private SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

    private ArrayList<String> courseTitles = new ArrayList<String>();

    private int startDay, startMonth, startYear;
    private int endDay, endMonth, endYear;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_term_activity);

        titleEditText = (EditText) this.findViewById(R.id.titleEditText);
        startDateTextView = (TextView) this.findViewById(R.id.startDateTextView);
        endDateTextView = (TextView) this.findViewById(R.id.endDateTextView);
        coursesListView = (ListView) this.findViewById(R.id.courseListView);

        List<Course> validCourses = Course.find(Course.class, "TERM_ID = ?", String.valueOf(-1));

        for(int i = 0; i < validCourses.size(); i++){
            String name = validCourses.get(i).getTitle();
            courseTitles.add(name);
        }

        ArrayAdapter<String> coursesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, courseTitles);
        coursesListView.setAdapter(coursesAdapter);
        coursesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        coursesListView.setOnTouchListener(new View.OnTouchListener() {
            // makes the list view work within the scroll view
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        final DatePickerDialog.OnDateSetListener startDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDay = dayOfMonth;
                startMonth = monthOfYear+1;
                startYear = year;
                updateStartDate();
            }
        };

        startDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddTermActivity.this, startDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener endDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDay = dayOfMonth;
                endMonth = monthOfYear+1;
                endYear = year;
                updateEndDate();
            }
        };

        endDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddTermActivity.this, endDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateStartDate(){
        startDateTextView.setText(sdf.format(cal.getTime()));
    }

    private void updateEndDate(){
        endDateTextView.setText(sdf.format(cal.getTime()));
    }

    public void saveNewTerm(View v){

        String title = titleEditText.getText().toString();
        ArrayList<Course> selectedCourses = new ArrayList<Course>();
        List<Course> allCourses = Course.listAll(Course.class);

        int courseListLength = coursesListView.getCount();
        SparseBooleanArray checkedCourses = coursesListView.getCheckedItemPositions();
        for (int i = 0; i < courseListLength; i++){
            if (checkedCourses.get(i)) {
                String item = courseTitles.get(i);
                for (int j = 0; j < allCourses.size(); j++) {
                    if (item.toString().equals(allCourses.get(j).getTitle())) {
                        selectedCourses.add(allCourses.get(j));
                    }
                }
            }
        }

        Term term = new Term(title, startDay, startMonth, startYear, endDay, endMonth, endYear, selectedCourses);
        term.save();
        Intent myIntent = new Intent(getApplicationContext(), TermsActivity.class);
        startActivity(myIntent);
    }

}

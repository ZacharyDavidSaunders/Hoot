package com.example.zachary.hoot;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddAssessmentActivity extends AppCompatActivity{

    private EditText title;
    private Switch type;
    private TextView dueDate;
    private TextView goalDate;
    private CheckBox goalDateNotificationCheckBox;

    private boolean isObjective;
    private int goalDay, goalMonth, goalYear;
    private int dueDay, dueMonth, dueYear;
    private boolean goalDateNotification;

    Calendar cal = Calendar.getInstance();
    String dateFormat = "MM/dd/yy";
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_assessment_activity);
        title = (EditText) this.findViewById(R.id.titleEditText);
        type = (Switch) this.findViewById(R.id.addTypeToggle);
        dueDate = (TextView) this.findViewById(R.id.dueDateTextView) ;
        goalDate = (TextView) this.findViewById(R.id.goalDateTextView) ;
        goalDateNotificationCheckBox = (CheckBox)  this.findViewById(R.id.goalDateAlertcheckBox);

        final DatePickerDialog.OnDateSetListener dueDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dueDay = dayOfMonth;
                dueMonth = monthOfYear+1;
                dueYear = year;
                updateDueDate();
            }
        };

        dueDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddAssessmentActivity.this, dueDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener goalDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                goalDay = dayOfMonth;
                goalMonth = monthOfYear+1;
                goalYear = year;
                updateGoalDate();
            }
        };

        goalDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddAssessmentActivity.this, goalDatePicker, cal
                        .get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void saveNewAssessment(View v){

        if(type.isChecked()){
            isObjective = false;
        }else{
            isObjective = true;
        }

        if(goalDateNotificationCheckBox.isChecked()){
            goalDateNotification = true;
        }else{
            goalDateNotification = false;
        }

        System.out.println("isObjective: "+isObjective);
        System.out.println("title: "+title.getText().toString());

        System.out.println("goal month: "+goalMonth);
        System.out.println("goal day: "+goalDay);
        System.out.println("goal year: "+goalYear);

        System.out.println("due month: "+dueMonth);
        System.out.println("due day: "+dueDay);
        System.out.println("due year: "+dueYear);

        Assessment assessment = new Assessment(title.getText().toString(), isObjective, goalDay, goalMonth, goalYear, dueDay, dueMonth, dueYear, goalDateNotification);
        assessment.save();
        Intent myIntent = new Intent(this, AssessmentsActivity.class);
        startActivity(myIntent);
    }

    private void updateDueDate(){
        dueDate.setText(sdf.format(cal.getTime()));
    }
    private void updateGoalDate(){
        goalDate.setText(sdf.format(cal.getTime()));
    }

}

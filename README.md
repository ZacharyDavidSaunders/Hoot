
# Hoot

<img src="https://i.imgur.com/oMACuB4.png" alt="Hoot">

Hoot is an Android Application for WGU students. [View it on the Google Play Store by clicking here!](https://play.google.com/store/apps/details?id=com.wgu.zachary.hoot) This application was originally a school project and it earned me an Excellence Award from my university.

## Screenshots:
**_Note: Not all screens are shown._**

<img src="https://i.imgur.com/8z2OYwN.jpg" alt="Home Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/A73U2Ze.jpg" alt="Mentors Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/CFQ0zh6.jpg" alt="Add Mentor Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/vAzzP2w.jpg" alt="Modify Mentor Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/7XjNLuj.jpg" alt="Assessments Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/uY8X6lp.jpg" alt="Add Assessment Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/3JFd87L.jpg" alt="Modify Assessment Screen" width="200" height="330" align="left">
<img src="https://i.imgur.com/fR4bUWu.jpg" alt="Courses Screen" width="200" height="330">

## Google Play Stats:
* As of writing, the app has 26 installs and all 5 star reviews.
* No users have experienced ANRs nor crashes.
* 92.86% of the installs are on American devices.

## Technologies Used:
* Android
* SQLite database
* Sugar ORM (for working with the SQLite database)
* Java

## Release History:
* 1.0: Initial Release
